# DISPLAX C# (Unity framework) SDK - Active OR support

This C# SDK is a small library, targeted to C# for Unity, that provides a simple abstraction layer on top of Displax Service API.

The primary purpose of this SDK is to ease the integration of DISPLAX Active Object Recognition solution with Unity development system.

All the DISPLAX Service functionalities can be explored by extending the middleware communication classes available in this SDK, following the Service communication protocol (see _Displax C++/Qt SDK & Examples_ repository for more details). 

This SDK includes the library source code and an example. 

The development of this SDK follows DISPLAX code style convention, but also some c# code style conventions whenever is was needed. 

* [License](license.txt)

* [What is DISPLAX Active OR?](Documentation/DisplaxActiveOR_whatIs.md#what-is-displax-active-or)
    * [SDK dependencies](Documentation/DisplaxActiveOR_whatIs.md#sdk-dependencies)
	* [System and Communication model](Documentation/DisplaxActiveOR_whatIs.md#system-concepts-and-communication-model)

* [DPXActiveOR library](Documentation/DPXActiveOR_library.md#dpxactiveor-library)
	* [How to use it](Documentation/DPXActiveOR_library.md#-how-to-use-dpxactiveor-library)
    * [Concepts](Documentation/DPXActiveOR_Concepts.md)
      * [Reactions and Actions](Documentation/DPXActiveOR_Concepts.md#reactions-and-actions)
      * [Normal and Joystick Modes](Documentation/DPXActiveOR_Concepts.md#normal-and-joystick-modes)
    * [DPXActiveOREngine class](Documentation/DPXActiveOR_DPXActiveOREngine.md)
      * [Properties](Documentation/DPXActiveOR_DPXActiveOREngine.md#properties)
      * [Events](Documentation/DPXActiveOR_DPXActiveOREngine.md#events)
      * [Methods](Documentation/DPXActiveOR_DPXActiveOREngine.md#methods)
    * [Data Classes](Documentation/DPXActiveOR_Classes.md)
      * [DPXActiveTag (Reaction)](Documentation/DPXActiveOR_Classes.md#dpxactivetag)
        
* [Example](Documentation/DPXActiveOR_UnityExample.md)
