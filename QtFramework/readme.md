# Displax C++ (Qt framework) SDK & Examples

Displax C++ (Qt framework) SDK is a small library, intended for application development using C++ and Qt 5 framework, that provides access to Displax Service.

This SDK provide library source code and simple examples.
The project is in qmake.

* [License](license.txt)
* [Library and protocol specification.](client-lib/readme.md)

__Dependencies__:
* C++ compiler
* [Qt framework](https://www.qt.io/)
* QtCreator

__Examples__:
* [Basic Print Device Info](BasicPrintDeviceInfo/readme.md)
* [Device Reset](DeviceReset/readme.md)
* [Device Load Save Settings](DeviceLoadSaveSettings/readme.md)
* [Network Device Info](NetworkDeviceInfo/readme.md)
* [Network Device Info Extended](NetworkDeviceInfoExtended/readme.md)
* [Device Frame Touch](DeviceFrameTouch/readme.md)
* [Active Tags](ActiveTags/readme.md)

> :warning: **You must try examples sequentially to gain knowledge about how the system works**

> :warning: **Displax Service isn't compatible with Qt 5.15.**
