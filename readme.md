# Displax SDK & Examples

Displax SDK is a bundle of small libraries that provides access to Displax Service.

* [License](QtFramework/license.txt)

This bundle supports the following frameworks:

- [C++ (for Qt framework)](QtFramework/readme.md)
- [CSharp (for Unity framework)](CSharp%20(Unity%20framework)/readme.md)

A library sends requests and receive notifications from Displax Service API. These notifications and requests are JSON messages, with data structures defined by Displax Service API communication protocol.

For details on the communication protocol, please check: 

- [Library and protocol specification.](QtFramework/client-lib/readme.md)

